# spring-security-oauth2

## What is Oauth 2 ?
OAuth 2.0 is the industry-standard protocol for authorization. OAuth 2.0 supersedes the work done on the original OAuth protocol created in 2006. OAuth 2.0 focuses on client developer simplicity while providing specific authorization flows for web applications, desktop applications, mobile phones, and living room devices. This specification and its extensions are being developed within the IETF OAuth Working Group.

## Oauth 2 roles
OAuth2 defines 4 roles :

* __Resource Owner__: generally yourself.
* Resource Server: server hosting protected data (for example Google hosting your profile and personal information).
* __Client__: application requesting access to a resource server ( website, a Javascript application or a mobile application…).
* __Authorization Server__: server issuing access token to the client. This token will be used for the client to request the resource server. This server can be the same as the resource server (same physical server and same application), and it is often the case.

the following figure illustrates the roles flow :

![Outh2 flow](https://cdn-images-1.medium.com/max/1000/1*LrU0kZBljjwd32vLy5IlwA.png)

## Grant Types
  OAuth 2 provides several “grant types” for different use cases. The grant types defined are:

  * __Authorization Code__ : The authorization code grant is the feature of signing into an application using your Facebook or Google account.

  ![](http://www.bubblecode.net/wp-content/uploads/2013/03/auth_code_flow.png)

  * __Password__: It is intended to be used for user-agent-based clients (e.g. single page web apps) that can’t keep a client secret.Secondly instead of the authorization server returning an authorization code which is exchanged for an access token as the case of Authorization Code grant, the authorization server returns an access token.

  ![](http://www.bubblecode.net/wp-content/uploads/2013/03/password.png)

  * __Client credentials__ : The client can request an access token using only its client credentials (or other supported means of authentication) when the client is requesting access to the protected resources under its control, or those of another resource owner that have been previously arranged with the authorization server.

  ![](http://www.bubblecode.net/wp-content/uploads/2013/03/client_credentials_flow.png)

  * __Implicit__: The implicit grant is a simplified authorization code flow optimized for clients implemented in a browser using a scripting language such as JavaScript. In the implicit flow, instead of issuing the client an authorization code, the client is issued an access token directly.

  ![](http://www.bubblecode.net/wp-content/uploads/2013/03/implicit_flow.png)


# Context : Secure the product API
we will see how to use Spring Boot 2 with Spring Security OAuth2 to implement an authorization server for centralized authorization and how to administrate it through a GUI also a resource server (product API) will be tested.

## Steps:

* 1- Configure the mysql database (dbname : oauth2)
* 2- Create an Authorization Server.
* 3- Create a Resource Server.
* 4- Get a secured Resource using an access token using postman.


## STEP 1 : Configure the DataBase

* Install, configure and start mysql server
* add user __"root"__ with password __root__ --> change username and password according to your own mysql configration
* Create a new database collection named oauth2
* Grant all roles to user ( root )

NOTA : refer to https://dev.mysql.com/doc/workbench/en/wb-getting-started-tutorial-creating-a-model.html


## STEP 2 : Authorization Server (port : 8081)

The authorization server is responsible for the verification of user identity and providing the tokens ,the use of __@EnableAuthorizationServer__ annotation enables the Authorization server configuration

### 1- Import the project

- Download source code from : https://framagit.org/RAzzabi/spring-security-oauth2
- Import the Authorization Server project to Netbeans IDE
- Configure the JDBC connector (/resources/applicaiton.yaml) based on your mysql configuraiton
```
spring:
    datasource:
        hikari:
            connection-test-query: SELECT 1 FROM DUAL
            minimum-idle: 1
            maximum-pool-size: 5
        driver-class-name: com.mysql.jdbc.Driver
        jdbc-url: jdbc:mysql://localhost:3306/oauth2?serverTimezone=UTC&useLegacyDatetimeCode=false
        username: root
        password: root
        initialization-mode: always
    jpa:
      hibernate:
        ddl-auto: none
        jdbc:
           time_zone = TimeZone
# --- server
server:
  port: 8081
```



### 2- Setup clients models
To set up Oauth 2 clients we need to create the following tables

* OAUTH_CLIENT_DETAILS
* OAUTH_CLIENT_TOKEN
* OAUTH_ACCESS_TOKEN
* OAUTH_REFRESH_TOKEN
* OAUTH_CODE
* OAUTH_APPROVALS

add the sql schema
- location : /resources
- name : schema.sql

```
drop table if exists oauth_client_token;
create table oauth_client_token (
  token_id VARCHAR(255),
  token LONGBLOB,
  authentication_id VARCHAR(255),
  user_name VARCHAR(255),
  client_id VARCHAR(255)
);
drop table if exists oauth_client_details;
CREATE TABLE oauth_client_details (
  client_id varchar(255) NOT NULL,
  resource_ids varchar(255) DEFAULT NULL,
  client_secret varchar(255) DEFAULT NULL,
  scope varchar(255) DEFAULT NULL,
  authorized_grant_types varchar(255) DEFAULT NULL,
  web_server_redirect_uri varchar(255) DEFAULT NULL,
  authorities varchar(255) DEFAULT NULL,
  access_token_validity integer(11) DEFAULT NULL,
  refresh_token_validity integer(11) DEFAULT NULL,
  additional_information varchar(255) DEFAULT NULL,
  autoapprove varchar(255) DEFAULT NULL
);
drop table if exists oauth_access_token;
create table `oauth_access_token` (
  token_id VARCHAR(255),
  token LONGBLOB,
  authentication_id VARCHAR(255),
  user_name VARCHAR(255),
  client_id VARCHAR(255),
  authentication LONGBLOB,
  refresh_token VARCHAR(255)
);
drop table if exists oauth_refresh_token;
create table `oauth_refresh_token`(
  token_id VARCHAR(255),
  token LONGBLOB,
  authentication LONGBLOB
);
drop table if exists authority;
CREATE TABLE authority (
  id  integer,
  authority varchar(255),
  primary key (id)
);
drop table if exists credentials;

CREATE TABLE credentials (
  id  integer,
  enabled boolean not null,
  name varchar(255) not null,
  password varchar(255) not null,
  version integer,
  primary key (id)
);
drop table if exists credentials_authorities;
CREATE TABLE credentials_authorities (
  credentials_id bigint not null,
  authorities_id bigint not null
);
drop table if exists oauth_code;
create table oauth_code (
  code VARCHAR(255), authentication VARBINARY(255)
);
drop table if exists oauth_approvals;
create table oauth_approvals (
    userId VARCHAR(255),
    clientId VARCHAR(255),
    scope VARCHAR(255),
    status VARCHAR(10),
    expiresAt DATETIME,
    lastModifiedAt DATETIME
);
```

### 3- Authorities and Users Setup
Spring Security comes with two useful interfaces:

* UserDetails — provides core user information.
* GrantedAuthority — represents an authority granted to an Authentication object.

we define 3 Users

| user | password | role |
|------|----------|------|
| oauth_admin | user |  ROLE_OAUTH_ADMIN |
| resource_admin | user |  ROLE_ADMIN_PRODUCT |
| user | user |  ROLE_RESOURCE_ADMIN |

below is the script that will load all authorities and credentials (users ):

create the default data file
* location : /resources/
* name : data.sql
```
INSERT INTO authority  VALUES(1,'ROLE_OAUTH_ADMIN');
INSERT INTO authority VALUES(2,'ROLE_ADMIN_PRODUCT');
INSERT INTO authority VALUES(3,'ROLE_RESOURCE_ADMIN');
INSERT INTO credentials VALUES(1,b'1','oauth_admin','$2a$10$BurTWIy5NTF9GJJH4magz.9Bd4bBurWYG8tmXxeQh1vs7r/wnCFG2','0');
INSERT INTO credentials VALUES(2,b'1','resource_admin','$2a$10$BurTWIy5NTF9GJJH4magz.9Bd4bBurWYG8tmXxeQh1vs7r/wnCFG2','0');
INSERT INTO credentials  VALUES(3,b'1','user','$2a$10$BurTWIy5NTF9GJJH4magz.9Bd4bBurWYG8tmXxeQh1vs7r/wnCFG2','0');
INSERT INTO credentials_authorities VALUES (1, 1);
INSERT INTO credentials_authorities VALUES (2, 3);
INSERT INTO credentials_authorities VALUES (3, 2);
```

### 4- Authorization for product_api
we will call a resource server like ‘product_api’ For this server, we define one client called:

read-write-client (authorized grant types: read, write)
```
INSERT INTO OAUTH_CLIENT_DETAILS(CLIENT_ID, RESOURCE_IDS, CLIENT_SECRET, SCOPE, AUTHORIZED_GRANT_TYPES, AUTHORITIES, ACCESS_TOKEN_VALIDITY, REFRESH_TOKEN_VALIDITY)
VALUES ('read-write-client', 'product-api','$2a$10$BurTWIy5NTF9GJJH4magz.9Bd4bBurWYG8tmXxeQh1vs7r/wnCFG2','read,write', 'client_credentials', 'ROLE_PRODUCT_ADMIN', 10800, 2592000);
```` 

add Athaurization to data.sql

### 4- Authorization Server
* 1- clen and build
* 2- Resolve dependencies problems
* 3- Run the project
* 4- Test end point : http://127.0.0.1:8081/ use login : __oauth_admin__ , password : __user__


### 5- Questions

* 1- Explore the source code
* 2- Describe the list of avaible package and each content ?
* 3- Explain how the password are stored in the DataBase, which algorithm is used ?  


## STEP 2 : Resource Server (port : 8083)
A Resource Server hosts resources that are protected by the OAuth2 token (basically it is our Product API )

### 1- start the product API
* 1- import the Resource server as NetBeans project
* 2- configure DataBase : Configure the JDBC connector (/resources/applicaiton.yaml) based on your mysql configuraiton , create a new database or use the same database (oauth2) as the authorization server)
```
spring:
    datasource:
        hikari:
            connection-test-query: SELECT 1 FROM DUAL
            minimum-idle: 1
            maximum-pool-size: 5
        driver-class-name: com.mysql.jdbc.Driver
        jdbc-url: jdbc:mysql://localhost:3306/oauth2?serverTimezone=UTC&useLegacyDatetimeCode=false
        username: root
        password: root
        initialization-mode: always
    jpa:
      hibernate:
        ddl-auto: none
        jdbc:
           time_zone = TimeZone
server:
  port: 8083
```

* 3- clean and build
* 4- run the product API
* 5- test endpoints with postman : localhost:8083/product/products

NOTA : in order to obtain the access-token from the Authorization server:
 * 1- use postman with Authentication type oauth2
 * 2- choose generate new token
 * 3- configure request for new token =>

  - user : curl_client
  - passord : user

NOTA : new user __curl_client__ need to be added befor 

 *1- methode 1 : add script to data.sql
```
INSERT INTO oauth_client_details VALUES('curl_client','product_api', '$2a$10$BurTWIy5NTF9GJJH4magz.9Bd4bBurWYG8tmXxeQh1vs7r/wnCFG2', 'read,write', 'client_credentials', 'http://127.0.0.1', 'ROLE_PRODUCT_ADMIN', 7200, 0, NULL, 'true');
```
  *2- use Oauth Dashboard ( Authorisation server at http://127.0.0.1:8081/)

### 2- Questions
* 1- Explore the source code of the product API
* 2- Describe the product API (functionality, describe the data models, endpoints etc ...)
